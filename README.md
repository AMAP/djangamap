DjangAmap est un site vitrine pour le réseau fictif AMAP Auvergne, réalisé avec Django CMS.

On commence par initialiser un environnement de développement :
```
python3 -m venv djangamap_env
source djangamap_env/bin/activate
```

Puis, on met à jour `pip` :
```
pip install --upgrade pip
```

On clone alors le projet (par exemple en SSH) :
```
git clone git@framagit.org:AMAP/djangamap.git
```

On initialise notre environnement Python :
```
cd djangamap
pip install -r requirements_gen.txt
```

On initialise la structure de la base de données (sqlite par défaut) :
```
python manage.py migrate
```

Si on a sauvegardé les données du projet, on peut importer ces données :
```
python manage.py loaddata db.json
```

Ou alors, on crée le premier utilisateur, qui sera également le compte du webmestre :
```
python manage.py createsuperuser
```

On lance enfin le serveur de développement :
```
python manage.py runserver
```
